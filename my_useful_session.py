# coding: utf-8
import matplotlib.pyplot as plt
import numpy as np
import cv2
from scipy.ndimage import label
import os
import pydub

fin = "20150430_233836.mp4"

capture = cv2.VideoCapture(fin)

length = int(capture.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT))
w = int(capture.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH))
h = int(capture.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))
fps = capture.get(cv2.cv.CV_CAP_PROP_FPS)

fr = np.zeros((length, w, h), dtype = 'uint8')
n = -1
while True:
    ret, img = capture.read()
    if not ret:
        break
    n = n+1
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    fr[n, : ,:]=cv2.flip(np.transpose(img,(1,0)), 1)

meanfr = np.mean(fr, axis=0)
maxfr = np.max(fr, axis=0)
mfr = maxfr-meanfr
hist, bin_edges = np.histogram(mfr, bins=25)
bin_centers= (bin_edges[1:]+bin_edges[:-1])/2

plt.figure()
plt.plot(bin_centers, hist)
plt.ylim(0,1000)

thresh, tfr = cv2.threshold(mfr.astype('uint8'), 160, 0, cv2.THRESH_TRUNC)
thresh, tfr = cv2.threshold(tfr, 60, 255, cv2.THRESH_BINARY)

se = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5,5))
dfr = cv2.dilate(tfr, se, iterations=5)

lbl, ncc = label(dfr)

signal = np.zeros((lbl.max(), length))
for i in range(length):
    frame = fr[i,:,:];
    for lblnum in range(lbl.max()):
        signal[lblnum, i] = np.max(frame[lbl==(lblnum+1)])

for i,s in enumerate(signal):
    plt.figure()
    plt.plot(s)
    plt.title(str(i))

tt = np.array(range(length))
tt = tt.reshape((length,1))
events = np.zeros(tt.shape)
channels = [2,3,6]

for c in channels:
    channel = signal[c, :];
    _, tc = cv2.threshold(channel.astype('uint8'), thresh, 255, cv2.THRESH_BINARY)
    lc, nc = label(tc);
    for i in range(1, nc+1):
        events[int(np.mean(tt[lc==i]))]=1

files = ['./workit.mp3', './makeit.mp3', './doit.mp3', './makesus.mp3', './harder.mp3', './better.mp3', './faster.mp3', './stronger.mp3', './morethan.mp3', './hour.mp3', './our.mp3', './never.mp3', './ever.mp3', './after.mp3', './workis.mp3', './over.mp3']
chops = map(lambda f: pydub.AudioSegment.from_mp3(f), files)

events = events.reshape(-1)
time = np.round(np.array(range(length))*1000./fps).astype('uint8')
chopstarts= time[events>0]
song = pydub.AudioSegment.silent(duration=max(time)*1000)

for chopnum, start in enumerate(chopstarts):
    chop = chops[chopnum % len(chops)]
    song = song.overlay(chop, position = start)

song.export(os.path.splitext(fin)[0]+'.mp3')

# ffmpeg -i {{VIDEONAME}} -i {{SONGNAME}} -c copy -map 0:0 -map 1:0 -shortest output.mp4









